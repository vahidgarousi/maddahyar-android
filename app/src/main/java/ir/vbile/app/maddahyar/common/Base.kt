package ir.vbile.app.maddahyar.common

import android.content.Context
import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.annotation.MainThread
import androidx.appcompat.app.AppCompatActivity
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.createViewModelLazy
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStoreOwner
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.squareup.moshi.JsonClass
import ir.vbile.app.maddahyar.R
import ir.vbile.app.maddahyar.common.LoadingState.Companion.error
import ir.vbile.app.maddahyar.common.Status.*
import ir.vbile.app.maddahyar.common.Resource
import ir.vbile.app.maddahyar.util.LocaleManager
import retrofit2.HttpException
import kotlin.reflect.KClass

abstract class BaseFragment<VM : BaseViewModel>(
    @LayoutRes protected val layoutRes: Int,
    private val vmClass: KClass<VM>
) : Fragment(), BaseView {

    override val rootView: CoordinatorLayout?
        get() = view as CoordinatorLayout?

    override val viewContext: Context?
        get() = context

    open fun getViewModelStoreOwner(): ViewModelStoreOwner = this
    protected val vm: VM by viewModels({ getViewModelStoreOwner() })
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(layoutRes, container, false)

    @MainThread
    private fun viewModels(
        ownerProducer: () -> ViewModelStoreOwner = { this },
        factoryProducer: (() -> ViewModelProvider.Factory)? = null
    ) = createViewModelLazy(vmClass, { ownerProducer().viewModelStore }, factoryProducer)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        requireActivity().window.decorView.layoutDirection = View.LAYOUT_DIRECTION_RTL
        assignArguments()
        setupViews(view)
        subscribeToObservers()
        super.onViewCreated(view, savedInstanceState)
    }
}

abstract class BaseActivity : AppCompatActivity(), BaseView {
    override val rootView: CoordinatorLayout?
        get() = window.peekDecorView() as CoordinatorLayout?
    override val viewContext: Context?
        get() = this

    override fun onConfigurationChanged(newConfig: Configuration) {
        LocaleManager.setNewLocale(this, LocaleManager.PERSIAN)
        super.onConfigurationChanged(newConfig)
    }

    override fun attachBaseContext(newBase: Context?) {
        newBase?.let {
            super.attachBaseContext(LocaleManager.setLocale(it))
        }
    }
}

interface BaseView {
    val rootView: CoordinatorLayout?
    val viewContext: Context?
    fun setProgressIndicator(mustShow: Boolean) {
        rootView?.let {
            viewContext?.let { context ->
                var loadingView = it.findViewById<View>(R.id.loading_view)
                if (loadingView == null && mustShow) {
                    loadingView =
                        LayoutInflater.from(context).inflate(R.layout.view_loading, it, false)
                    it.addView(loadingView)
                }
                loadingView?.visibility = if (mustShow) View.VISIBLE else View.GONE
            }
        }
    }

    fun assignArguments() {}
    fun setupViews(view: View) = view.apply { }
    fun subscribeToObservers() {}
    fun showError(message: String) {
        rootView?.let {
            viewContext?.let { _ ->
                Snackbar.make(it, message, Snackbar.LENGTH_LONG).show()
            }
        }
    }
    fun showSuccess(message: String) {
        rootView?.let {
            viewContext?.let { _ ->
                Snackbar.make(it, message, Snackbar.LENGTH_LONG).show()
            }
        }
    }
}

interface BaseRepository {
    suspend fun <T> safeApiCall(call: suspend () -> T): Resource<T> {
        return try {
            Resource.Success(call.invoke())
        } catch (exp: Exception) {
            when (exp) {
                is HttpException -> Resource.Error(exp)
                else -> Resource.Error(exp)
            }
        }
    }

    private fun processRetrofit(exception: HttpException): Exception {
        return when (exception.code()) {
            401 -> Unauthorized()
            else -> Exception()
        }
    }
}


abstract class BaseViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun bind() {
        if (absoluteAdapterPosition != RecyclerView.NO_POSITION) bind(bindingAdapterPosition)
    }

    protected abstract fun bind(position: Int)
}

class Unauthorized() : Exception()


abstract class BaseViewModel : ViewModel() {
    val progressLiveData: MutableLiveData<Boolean> = MutableLiveData()
}

@JsonClass(generateAdapter = true)
data class ResponseBody<T>(
    val status: String,
    val data: T,
    val code: String
)

@JsonClass(generateAdapter = true)
data class ArrayResponseBody<T>(
    val status: String,
    val data: List<T>,
    val code: String
)

/**
 * A generic class that holds a value with its loading status.
 * @param <T>
 */
sealed class Resource<out R> {

    data class Success<out T>(val data: T) : Resource<T>()
    data class Error(val exception: Exception, val mustReturn: Boolean = false) :
        Resource<Nothing>()

    data class Loading(var loadingState: LoadingState) : Resource<LoadingState>()

    override fun toString(): String {
        return when (this) {
            is Success<*> -> "Success[data=$data]"
            is Error -> "Error[exception=$exception]"
            is Loading -> "Loading"
        }
    }
}

/**
 * `true` if [Result] is of type [Success] & holds non-null [Success.data].
 */
val Resource<*>.succeeded get() = this is Resource.Success && data != null

fun <T> Resource<T>.successOr(fallback: T): T {
    return (this as? Resource.Success<T>)?.data ?: fallback
}

val <T> Resource<T>.data: T?
    get() = (this as? Resource.Success)?.data

/**
 * Updates value of [liveData] if [Result] is of type [Success]
 */
inline fun <reified T> Resource<T>.updateOnSuccess(liveData: MutableLiveData<T>): Resource<T> {
    if (this is Resource.Success) {
        liveData.postValue(data)
    }
    return this
}

inline fun <reified T> Resource<T>.onSuccess(block: MethodBlock<T>): Resource<T> {
    if (this is Resource.Success) {
        block(data)
    }
    return this
}

inline fun <reified T> Resource<T>.onError(block: MethodBlock<Exception>): Resource<T> {
    if (this is Resource.Error) {
        block(exception)
    }
    return this
}

inline fun <reified T> Resource<T>.onEvent(block: FunctionBlock) {
    if (this is Resource.Error) {
        block()
    }
    if (this is Resource.Success) {
        block()
    }
}
