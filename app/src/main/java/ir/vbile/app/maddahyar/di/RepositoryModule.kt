package ir.vbile.app.maddahyar.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import ir.vbile.app.maddahyar.BuildConfig
import ir.vbile.app.maddahyar.data.repo.category.CategoryRepository
import ir.vbile.app.maddahyar.data.repo.category.CategoryRepositoryImpl
import ir.vbile.app.maddahyar.data.repo.category.DemoCategoryRepositoryImpl
import ir.vbile.app.maddahyar.data.repo.lyric.DemoLyricRepositoryImpl
import ir.vbile.app.maddahyar.data.repo.lyric.LyricRepository
import ir.vbile.app.maddahyar.data.repo.lyric.LyricRepositoryImpl
import ir.vbile.app.maddahyar.data.repo.source.category.CategoryLocalDataSource
import ir.vbile.app.maddahyar.data.repo.source.category.CategoryRemoteDataSource
import ir.vbile.app.maddahyar.data.repo.source.lyric.LyricLocalDataSource
import ir.vbile.app.maddahyar.data.repo.source.lyric.LyricRemoteDataSource
import ir.vbile.app.maddahyar.services.http.CategoryApi
import ir.vbile.app.maddahyar.services.http.LyricsApi
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object RepositoryModule {

    @Provides
    @Singleton
    fun provideLyricsRepository(
        lyricsApi: LyricsApi
    ): LyricRepository {
        return if (!BuildConfig.DEMO_MODE)
            LyricRepositoryImpl(
                LyricRemoteDataSource(lyricsApi),
                LyricLocalDataSource()
            )
        else
            DemoLyricRepositoryImpl()
    }


    @Provides
    @Singleton
    fun provideCategoryRepository(
        categoryApi: CategoryApi
    ): CategoryRepository {
        return if (!BuildConfig.DEMO_MODE)
            CategoryRepositoryImpl(
                CategoryRemoteDataSource(categoryApi),
                CategoryLocalDataSource()
            )
        else
            DemoCategoryRepositoryImpl()
    }
}