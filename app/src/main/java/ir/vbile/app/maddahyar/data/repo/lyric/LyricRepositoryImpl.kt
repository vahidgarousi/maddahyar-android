package ir.vbile.app.maddahyar.data.repo.lyric

import ir.vbile.app.maddahyar.common.Resource
import ir.vbile.app.maddahyar.data.local.Lyric
import ir.vbile.app.maddahyar.data.repo.source.lyric.LyricDataSource
import javax.inject.Inject

class LyricRepositoryImpl @Inject constructor(
    private val remoteLyricRepository: LyricDataSource,
    private val localLyricRepository: LyricDataSource
) : LyricRepository {
    override suspend fun getLyrics(): Resource<List<Lyric>> = safeApiCall {
        remoteLyricRepository.getLyrics()
    }

    override suspend fun search(keyword: String): Resource<List<Lyric>> = safeApiCall {
        remoteLyricRepository.search(keyword)
    }

    override suspend fun getLyricsByCategoryId(categoryId: String): Resource<List<Lyric>> =
        safeApiCall {
            remoteLyricRepository.getLyricsByCategoryId(categoryId)
        }

    override suspend fun createLyric(title: String, content: String, categoryId: String): Resource<Lyric> = safeApiCall{
        remoteLyricRepository.createLyric(title,content,categoryId)
    }
}