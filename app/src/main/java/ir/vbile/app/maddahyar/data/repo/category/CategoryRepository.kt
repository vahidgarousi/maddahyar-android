package ir.vbile.app.maddahyar.data.repo.category

import ir.vbile.app.maddahyar.common.BaseRepository
import ir.vbile.app.maddahyar.common.Resource
import ir.vbile.app.maddahyar.data.local.Category

interface CategoryRepository : BaseRepository {
    suspend fun getCategories(): Resource<List<Category>>
}