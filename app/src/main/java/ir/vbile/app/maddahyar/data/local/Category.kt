package ir.vbile.app.maddahyar.data.local

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize

data class Category(
    val id: Int?,
    val title: String?,
    val image: String?
) : Parcelable {

}
