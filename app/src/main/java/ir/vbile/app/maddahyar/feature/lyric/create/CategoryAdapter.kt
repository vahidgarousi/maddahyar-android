package ir.vbile.app.maddahyar.feature.lyric.create

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import ir.vbile.app.maddahyar.R
import ir.vbile.app.maddahyar.common.BaseViewHolder
import ir.vbile.app.maddahyar.data.local.Category
import kotlinx.android.synthetic.main.item_category.view.*
import javax.inject.Inject

class CategoryAdapter @Inject constructor() : ListAdapter<Category, BaseViewHolder>(diffUtil),
    Filterable {
    var filteredList: List<Category> = listOf()
    var unFilteredList: List<Category> = listOf()
    private var listener: (Category) -> Unit = {}
    fun setOnItemClickedListener(listener: (Category) -> Unit) {
        this.listener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return CategoryViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_category, parent, false)
        )
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) = holder.bind()

    inner class CategoryViewHolder(itemView: View) : BaseViewHolder(itemView) {
        override fun bind(position: Int) {
            val category = getItem(position) ?: return
            itemView.apply {
                tvCategoryTitle.text = category.title
                setOnClickListener {
                    listener.invoke(category)
                }
            }
        }
    }

    companion object {
        private val diffUtil = object : DiffUtil.ItemCallback<Category>() {
            override fun areItemsTheSame(oldItem: Category, newItem: Category): Boolean =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: Category, newItem: Category): Boolean =
                oldItem.hashCode() == newItem.hashCode()
        }
    }


    override fun getFilter(): Filter = object : Filter() {
        override fun performFiltering(constraint: CharSequence?): FilterResults {
            val charString: String = constraint.toString()
            var filteredList: MutableList<Category> = arrayListOf()
            if (charString.isEmpty()) {
                filteredList = unFilteredList as MutableList<Category>
            } else {
                for (row in currentList) {
                    if (row.title!!.contains(charString)) {
                        filteredList.add(row)
                    }
                }
            }

            val filterResults = FilterResults()
            filterResults.values = filteredList
            return filterResults
        }

        override fun publishResults(constraint: CharSequence?, filterResults: FilterResults?) {
            submitList(filterResults?.values as MutableList<Category>?)
            notifyDataSetChanged()
        }
    }
}