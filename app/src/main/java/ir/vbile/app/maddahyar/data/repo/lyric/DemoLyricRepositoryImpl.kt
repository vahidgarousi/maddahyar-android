package ir.vbile.app.maddahyar.data.repo.lyric

import ir.vbile.app.maddahyar.common.Resource
import ir.vbile.app.maddahyar.data.local.Lyric

class DemoLyricRepositoryImpl : LyricRepository {
    override suspend fun getLyrics(): Resource<List<Lyric>> {
        TODO("Not yet implemented")
    }

    override suspend fun search(keyword: String): Resource<List<Lyric>> {
        TODO("Not yet implemented")
    }

    override suspend fun getLyricsByCategoryId(categoryId: String) : Resource<List<Lyric>>{
        TODO("Not yet implemented")
    }
    override suspend fun createLyric(
        title: String,
        content: String,
        categoryId: String
    ): Resource<Lyric> {
        TODO("Not yet implemented")
    }
}