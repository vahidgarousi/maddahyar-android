package ir.vbile.app.maddahyar.di


import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import ir.vbile.app.maddahyar.BuildConfig
import ir.vbile.app.maddahyar.services.http.CategoryApi
import ir.vbile.app.maddahyar.services.http.LyricsApi
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object RestClientModule {

    @Provides
    @Singleton
    fun provideLyricsApi(retrofit: Retrofit): LyricsApi = retrofit.create(LyricsApi::class.java)


    @Provides
    @Singleton
    fun provideCategoryApi(retrofit: Retrofit): CategoryApi = retrofit.create(CategoryApi::class.java)


    @Provides
    @Singleton
    fun provideRetrofit(
        okHttpClient: OkHttpClient
    ): Retrofit = Retrofit.Builder()
        .baseUrl(BuildConfig.API_PRODUCTION_URL)
        .addConverterFactory(MoshiConverterFactory.create())
        .client(okHttpClient)
        .build()
}