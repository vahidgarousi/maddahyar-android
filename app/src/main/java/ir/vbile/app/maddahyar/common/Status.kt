package ir.vbile.app.maddahyar.common

enum class Status {
    LOADING,
    SUCCESS,
    EMPTY,
    FAILED,
    ERROR
}

@Suppress("DataClassPrivateConstructor")
data class LoadingState private constructor(
    var status: Status,
    val exp: Exception? = null
) {
    companion object {
        val EMPTY = LoadingState(Status.EMPTY) // New
        val LOADED = LoadingState(Status.SUCCESS)
        val LOADING = LoadingState(Status.LOADING)
        val INITIALIZING = LoadingState(Status.LOADING)
        fun error(exp: Exception?) = LoadingState(Status.FAILED, exp)
    }
}