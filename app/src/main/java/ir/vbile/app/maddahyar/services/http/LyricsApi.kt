package ir.vbile.app.maddahyar.services.http

import ir.vbile.app.maddahyar.common.ResponseBody
import ir.vbile.app.maddahyar.data.api.ResLyrics
import retrofit2.http.*

interface LyricsApi {
    @GET(EndPoints.getLyrics)
    suspend fun getLyrics(): ResponseBody<ResLyrics>

    @GET(EndPoints.search)
    suspend fun search(@Query("keyword") keyword: String): ResponseBody<ResLyrics>


    @GET(EndPoints.getLyricsByCategoryId)
    suspend fun getLyricsByCategoryId(
        @Query("category_id") categoryId: String
    ): ResponseBody<ResLyrics>

    @POST(EndPoints.createLyric)
    @FormUrlEncoded
    suspend fun createLyric(
        @Field("title") title: String,
        @Field("content") content: String,
        @Field("categoryId") categoryId: String
    ) : ResponseBody<ResLyrics.ApiLyric>
}