package ir.vbile.app.maddahyar.services.http

import ir.vbile.app.maddahyar.BuildConfig
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

class CommonHeadersInterceptor @Inject constructor() : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request().run {
            this.newBuilder()
                .addHeader("Content-Type", "application/json")
                .addHeader("Accept", "application/json")
                .addHeader("secret-key", BuildConfig.API_SECRET_KEY)
                .build()
        }
        return chain.proceed(request)
    }
}
