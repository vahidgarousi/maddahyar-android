package ir.vbile.app.maddahyar.data.repo.source.category

import ir.vbile.app.maddahyar.data.local.Category
import ir.vbile.app.maddahyar.services.http.CategoryApi
import javax.inject.Inject

class CategoryRemoteDataSource @Inject constructor(
    private val categoryApi: CategoryApi
) : CategoryDataSource {
    override suspend fun getCategories(): List<Category> =
        categoryApi.getCategories().data.toCategories()
}