package ir.vbile.app.maddahyar.services.http

import ir.vbile.app.maddahyar.common.ResponseBody
import ir.vbile.app.maddahyar.data.api.ResCategories
import retrofit2.http.GET

interface CategoryApi {
    @GET(EndPoints.getCategories)
    suspend fun getCategories(): ResponseBody<ResCategories>
}