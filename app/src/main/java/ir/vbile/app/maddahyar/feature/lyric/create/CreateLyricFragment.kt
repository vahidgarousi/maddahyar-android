package ir.vbile.app.maddahyar.feature.lyric.create

import android.view.View
import androidx.lifecycle.ViewModelStoreOwner
import androidx.navigation.fragment.findNavController
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import ir.vbile.app.maddahyar.R
import ir.vbile.app.maddahyar.common.BaseFragment
import ir.vbile.app.maddahyar.feature.main.MainVM
import ir.vbile.app.maddahyar.util.extentions.afterTextChanged
import ir.vbile.app.maddahyar.util.extentions.longToast
import kotlinx.android.synthetic.main.fragment_create_lyric.*
import javax.inject.Inject

@AndroidEntryPoint
class CreateLyricFragment : BaseFragment<MainVM>(
    R.layout.fragment_create_lyric,
    MainVM::class
) {
    override fun getViewModelStoreOwner(): ViewModelStoreOwner = parentFragment ?: this

    @Inject
    lateinit var categoryAdapter: CategoryAdapter

    private lateinit var standardBottomSheetBehavior: BottomSheetBehavior<View>

    override fun setupViews(view: View): View = view.apply {
        setupStandardBottomSheet()
        setupCategoryAdapter()
        etTitle.afterTextChanged {
            vm.setTitle(it)
        }
        etContent.afterTextChanged {
            vm.setContent(it)
        }
        btnCreateLyric.setOnClickListener {
            val title = etTitle.text.toString()
            val content = etContent.text.toString()
            if (title.length < 0) {
                etTitle.error = getString(
                    R.string.validate_required_value,
                    getString(R.string.required_value_title)
                )
                return@setOnClickListener
            }
            if (content.length < 0) {
                etTitle.error = getString(
                    R.string.validate_required_value,
                    getString(R.string.required_value_content)
                )
                return@setOnClickListener
            }
            vm.createLyric(title, content)
        }
        etCategory.setOnClickListener {
            standardBottomSheetBehavior.state = BottomSheetBehavior.STATE_HALF_EXPANDED
        }
        etKeyword.afterTextChanged { keyword->
            categoryAdapter.filter.filter(keyword)
        }
    }

    private fun setupCategoryAdapter() {
        categoryAdapter.setOnItemClickedListener {
            vm.setCategory(it)
            standardBottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
        }
        rvCategories.apply {
            adapter = categoryAdapter
        }
    }

    private fun setupStandardBottomSheet() {
        standardBottomSheetBehavior = BottomSheetBehavior.from(bShDialog)
        standardBottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
        val bottomSheetCallback = object : BottomSheetBehavior.BottomSheetCallback() {

            override fun onStateChanged(bottomSheet: View, newState: Int) {
                /*tvCategoryTitle.text = when (newState) {
                    BottomSheetBehavior.STATE_EXPANDED -> "STATE_EXPANDED"
                    BottomSheetBehavior.STATE_COLLAPSED -> "STATE_COLLAPSED"
                    BottomSheetBehavior.STATE_DRAGGING -> "STATE_DRAGGING"
                    BottomSheetBehavior.STATE_HALF_EXPANDED -> "STATE_HALF_EXPANDED"
                    BottomSheetBehavior.STATE_HIDDEN -> "STATE_HIDDEN"
                    BottomSheetBehavior.STATE_SETTLING -> "STATE_SETTLING"
                    else -> null
                }*/
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {}
        }
        standardBottomSheetBehavior.addBottomSheetCallback(bottomSheetCallback)
        standardBottomSheetBehavior.saveFlags = BottomSheetBehavior.SAVE_ALL
    }

    override fun subscribeToObservers() {
        vm.categories.observe(viewLifecycleOwner) {
            categoryAdapter.submitList(it)
            categoryAdapter.filteredList = it
            categoryAdapter.unFilteredList = it
        }
        vm.progressLiveData.observe(viewLifecycleOwner) {
            setProgressIndicator(it)
        }
        vm.category.observe(viewLifecycleOwner) {
            etCategory.setText(getString(R.string.edit_text_category_selected_value, it.title))
            longToast(it.title.toString())
        }
        vm.isLyricCreatedSuccessfully.observe(viewLifecycleOwner) {
            if (it) findNavController().popBackStack()
        }
    }
}