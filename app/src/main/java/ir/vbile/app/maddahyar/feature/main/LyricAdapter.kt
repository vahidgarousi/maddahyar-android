package ir.vbile.app.maddahyar.feature.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import ir.vbile.app.maddahyar.R
import ir.vbile.app.maddahyar.common.BaseViewHolder
import ir.vbile.app.maddahyar.data.local.Lyric
import kotlinx.android.synthetic.main.item_lyric_home.view.*
import javax.inject.Inject

class LyricAdapter @Inject constructor(

) : ListAdapter<Lyric, BaseViewHolder>(diffUtil) {

    private var listener: (Lyric) -> Unit = {}

    fun setOnItemClickedListener(listener: (Lyric) -> Unit) {
        this.listener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_lyric_home, parent, false)
        return LyricViewHolder(view)
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) = holder.bind()
    inner class LyricViewHolder(itemView: View) : BaseViewHolder(itemView) {
        override fun bind(position: Int) {
            val lyric = getItem(position) ?: return
            itemView.apply {
                setOnClickListener {
                    listener.invoke(lyric)
                }
                tvTitle.text = lyric.title
                tvContent.text = lyric.content
                tvCategoryTitle.text = lyric.category.title
                tvDate.text = context.getString(R.string.date)
            }
        }
    }

    companion object {
        private val diffUtil = object : DiffUtil.ItemCallback<Lyric>() {
            override fun areItemsTheSame(oldItem: Lyric, newItem: Lyric): Boolean =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: Lyric, newItem: Lyric): Boolean =
                oldItem.hashCode() == newItem.hashCode()
        }
    }
}