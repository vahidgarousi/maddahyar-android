package ir.vbile.app.maddahyar.feature.category

import android.view.View
import androidx.navigation.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import ir.vbile.app.maddahyar.R
import ir.vbile.app.maddahyar.common.BaseFragment
import ir.vbile.app.maddahyar.feature.main.MainVM
import ir.vbile.app.maddahyar.feature.main.SpacesItemDecoration
import ir.vbile.app.maddahyar.util.extentions.longToast
import kotlinx.android.synthetic.main.fragment_categories.*
import javax.inject.Inject
@AndroidEntryPoint
class CategoriesFragment : BaseFragment<MainVM>(
    R.layout.fragment_categories,
    MainVM::class
) {
    @Inject
    lateinit var categoriesAdapter: CategoriesAdapter


    @Inject
    lateinit var spacesItemDecoration: SpacesItemDecoration


    override fun setupViews(view: View): View = view.apply {
        categoriesAdapter.setOnItemClickedListener {
            val action = CategoriesFragmentDirections.actionCategoriesFragmentToLyricsFragment(it)
            findNavController().navigate(action)
        }
    }

    override fun subscribeToObservers() {
        vm.categories.observe(viewLifecycleOwner) {
            rvCategories.apply {
                layoutManager = GridLayoutManager(requireContext(), 2)
                adapter = categoriesAdapter
            }
            categoriesAdapter.submitList(it)
        }
    }
}