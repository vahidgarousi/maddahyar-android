package ir.vbile.app.maddahyar.data.repo.source.lyric

import ir.vbile.app.maddahyar.common.Resource
import ir.vbile.app.maddahyar.data.local.Lyric


interface LyricDataSource {
    suspend fun getLyrics(): List<Lyric>
    suspend fun search(keyword: String) : List<Lyric>
    suspend fun getLyricsByCategoryId(categoryId: String) : List<Lyric>
    suspend fun createLyric(title: String, content: String, categoryId: String) : Lyric
}