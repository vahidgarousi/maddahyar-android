package ir.vbile.app.maddahyar.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import ir.vbile.app.maddahyar.services.http.AuthorizationHeaderInterceptor
import ir.vbile.app.maddahyar.services.http.CommonHeadersInterceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor

@Module
@InstallIn(ApplicationComponent::class)
class OkHttpModule {

    @Provides
    internal fun provideLoggingInterceptor(): HttpLoggingInterceptor = HttpLoggingInterceptor()
        .apply { level = HttpLoggingInterceptor.Level.BODY }

    @Provides
    internal fun provideOkHttpClient(
        logging: HttpLoggingInterceptor,
        headerInterceptor: CommonHeadersInterceptor,
        authorizationHeaderInterceptor: AuthorizationHeaderInterceptor
    ): OkHttpClient = OkHttpClient.Builder()
        .addInterceptor(logging)
        .addInterceptor(headerInterceptor)
        .addInterceptor(authorizationHeaderInterceptor)
        .build()
}
