package ir.vbile.app.maddahyar.util

import android.content.Context
import android.content.res.Configuration
import android.content.res.Resources
import androidx.annotation.StringDef
import java.util.*

class LocaleManager {
    companion object {
        const val ENGLISH = "En"
        const val PERSIAN = "Fa"

        /**
         * SharedPreferences Key
         */
        private const val LANGUAGE_KEY = "SP_LANG"

        fun setLocale(context: Context): Context? {
            return updateResources(context, getLanguagePref(context))
        }


        /**
         * Set new Locale with context
         */
        fun setNewLocale(mContext: Context, @LocaleDef language: String): Context? {
            setLanguagePref(mContext, language)
            return updateResources(mContext, language)
        }

        /**
         * set pref key
         */
        private fun setLanguagePref(mContext: Context, localeKey: String) {
            val mPreferences = mContext.getSharedPreferences("SP_AUTH_TOKEN", Context.MODE_PRIVATE)
            mPreferences.edit().putString(LANGUAGE_KEY, localeKey).apply()
        }

        /**
         * Get saved Locale from SharedPreferences
         *
         * @param mContext current context
         * @return current locale key by default return english locale
         */
        private fun getLanguagePref(mContext: Context): String {
            mContext.getSharedPreferences("SP_AUTH_TOKEN", Context.MODE_PRIVATE)
            val mPreferences = mContext.getSharedPreferences("SP_AUTH_TOKEN", Context.MODE_PRIVATE)
            return mPreferences.getString(LANGUAGE_KEY, PERSIAN) ?: PERSIAN
        }


        /**
         * update resource
         */
        private fun updateResources(mContext: Context, language: String): Context? {
            var context = mContext
            val locale = Locale(language)
            Locale.setDefault(locale)
            val res = context.resources
            val config = Configuration(res.configuration)
            config.setLocale(locale)
            context = context.createConfigurationContext(config)
            return context
        }


    }

    @Retention(AnnotationRetention.SOURCE)
    @StringDef(
        ENGLISH, PERSIAN
    )
    annotation class LocaleDef {
        companion object {
            var SUPPORTED_LOCALES = arrayOf(
                ENGLISH,
                PERSIAN
            )
        }
    }
}