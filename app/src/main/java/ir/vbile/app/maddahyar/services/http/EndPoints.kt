package ir.vbile.app.maddahyar.services.http

object EndPoints {
    
    // Lyrics
    const val getLyrics = "lyrics"
    const val search = "lyrics/search"
    const val getLyricsByCategoryId = "lyrics"
    const val createLyric = "lyrics"


    // Categories
    const val getCategories = "categories"
}
