package ir.vbile.app.maddahyar.data.api


import kotlinx.android.parcel.Parcelize
import android.os.Parcelable
import com.squareup.moshi.JsonClass
import ir.vbile.app.maddahyar.data.local.Lyric

@Parcelize
@JsonClass(generateAdapter = true)
data class ResLyrics(
    val lyrics: List<ApiLyric>,
    val meta: Paginate
) : Parcelable {
    fun toLyrics() = lyrics.map { it.toLyric() }

    @Parcelize
    @JsonClass(generateAdapter = true)
    data class ApiLyric(
        val id: Int,
        val categories: ApiCategory?,
        val content: String,
        val title: String
    ) : Parcelable {
        fun toLyric() = Lyric(
            id, (categories ?: ApiCategory("1", "", "", "")).toCategory(), content, title
        )

        @Parcelize
        @JsonClass(generateAdapter = true)
        data class ApiCategory(
            val id: String,
            val title: String,
            val image: String,
            val desc: String?
        ) : Parcelable {
            fun toCategory() = Lyric.Category(
                id, image, title
            )

        }
    }

    @Parcelize
    @JsonClass(generateAdapter = true)
    data class Paginate(
        val currentPage: Int,
        val itemsCount: Int,
        val sizePerPage: Int,
        val totalPages: Int
    ) : Parcelable
}