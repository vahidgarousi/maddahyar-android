package ir.vbile.app.maddahyar.feature.lyric.list

import android.view.View
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.GridLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import ir.vbile.app.maddahyar.R
import ir.vbile.app.maddahyar.common.BaseFragment
import ir.vbile.app.maddahyar.feature.main.LyricAdapter
import ir.vbile.app.maddahyar.feature.main.MainFragmentDirections
import ir.vbile.app.maddahyar.feature.main.MainVM
import ir.vbile.app.maddahyar.feature.main.SpacesItemDecoration
import kotlinx.android.synthetic.main.fragment_lyrics.*
import javax.inject.Inject

@AndroidEntryPoint
class LyricsFragment : BaseFragment<MainVM>(
    R.layout.fragment_lyrics,
    MainVM::class
) {

    @Inject
    lateinit var lyricAdapter: LyricAdapter

    @Inject
    lateinit var spacesItemDecoration: SpacesItemDecoration

    private val args by navArgs<LyricsFragmentArgs>()

    override fun assignArguments() {
        vm.getLyricsByCategoryId(args.category.id.toString())
    }
    override fun setupViews(view: View): View = view.apply {
        setupLyricAdapter()
    }

    private fun setupLyricAdapter() {
        lyricAdapter.setOnItemClickedListener {
            val action = LyricsFragmentDirections.actionLyricsFragmentToShowLyricFragment(it)
            findNavController().navigate(action)
        }
        rvLyrics.apply {
            addItemDecoration(spacesItemDecoration)
            layoutManager = GridLayoutManager(requireContext(), 2)
            adapter = lyricAdapter
        }
    }

    override fun subscribeToObservers() {
        vm.lyricsFilteredByCategoryID.observe(viewLifecycleOwner) {
            lyricAdapter.submitList(it)
        }
    }
}