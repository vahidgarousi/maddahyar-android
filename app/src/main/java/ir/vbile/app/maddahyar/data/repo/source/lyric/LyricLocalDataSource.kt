package ir.vbile.app.maddahyar.data.repo.source.lyric

import ir.vbile.app.maddahyar.common.Resource
import ir.vbile.app.maddahyar.data.local.Lyric

class LyricLocalDataSource : LyricDataSource {
    override suspend fun getLyrics(): List<Lyric> {
        TODO("Not yet implemented")
    }

    override suspend fun search(keyword: String): List<Lyric> {
        TODO("Not yet implemented")
    }

    override suspend fun getLyricsByCategoryId(categoryId: String): List<Lyric> {
        TODO("Not yet implemented")
    }

    override suspend fun createLyric(
        title: String,
        content: String,
        categoryId: String
    ): Lyric {
        TODO("Not yet implemented")
    }
}