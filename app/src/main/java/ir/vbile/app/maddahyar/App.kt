package ir.vbile.app.maddahyar

import android.app.Application
import android.content.Context
import android.content.res.Configuration
import android.content.res.Resources
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.app.AppCompatDelegate.MODE_NIGHT_YES
import dagger.hilt.android.HiltAndroidApp
import ir.vbile.app.maddahyar.util.LocaleManager
import timber.log.Timber
import java.util.*

@HiltAndroidApp
class App : Application() {
    override fun onCreate() {
        super.onCreate()
        Timber.plant()
        AppCompatDelegate.setDefaultNightMode(MODE_NIGHT_YES)
    }


    override fun attachBaseContext(base: Context?) {
        base?.let {
            super.attachBaseContext(LocaleManager.setLocale(it))
        }
    }
    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        LocaleManager.setLocale(this)
    }
}