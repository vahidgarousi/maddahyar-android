package ir.vbile.app.maddahyar.data.repo.source.category

import ir.vbile.app.maddahyar.common.Resource
import ir.vbile.app.maddahyar.data.local.Category
import ir.vbile.app.maddahyar.data.local.Lyric


interface CategoryDataSource {
    suspend fun getCategories(): List<Category>
}