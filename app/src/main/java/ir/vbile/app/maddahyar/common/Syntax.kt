package ir.vbile.app.maddahyar.common

typealias MethodBlock<T> = T.() -> Unit
typealias FunctionBlock = () -> Unit
typealias AndroidPermission = android.Manifest.permission