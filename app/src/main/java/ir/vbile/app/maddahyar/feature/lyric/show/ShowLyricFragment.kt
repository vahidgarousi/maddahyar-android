package ir.vbile.app.maddahyar.feature.lyric.show

import android.view.View
import androidx.navigation.fragment.navArgs
import dagger.hilt.android.AndroidEntryPoint
import ir.vbile.app.maddahyar.R
import ir.vbile.app.maddahyar.common.BaseFragment
import ir.vbile.app.maddahyar.feature.main.MainVM
import kotlinx.android.synthetic.main.fragment_show_lyric.view.*

@AndroidEntryPoint
class ShowLyricFragment : BaseFragment<MainVM>(
    R.layout.fragment_show_lyric,
    MainVM::class
) {
    private val args by navArgs<ShowLyricFragmentArgs>()
    override fun assignArguments() {

    }

    override fun setupViews(view: View): View = view.apply {
        setProgressIndicator(true)
        etTitle.setText(args.lyric.title)
        etCategory.setText(
            getString(
                R.string.edit_text_category_selected_value,
                args.lyric.category.title
            )
        )
        etContent.setText(args.lyric.content)
        setProgressIndicator(false)
    }
}