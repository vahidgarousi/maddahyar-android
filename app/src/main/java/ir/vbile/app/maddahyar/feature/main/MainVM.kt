package ir.vbile.app.maddahyar.feature.main

import android.content.Context
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import androidx.paging.Config
import androidx.paging.DataSource
import androidx.paging.PageKeyedDataSource
import androidx.paging.toLiveData
import com.farsitel.bazaar.BazaarClientProxy
import com.farsitel.bazaar.auth.model.BazaarSignInAccount
import ir.vbile.app.maddahyar.common.*
import ir.vbile.app.maddahyar.data.local.Category
import ir.vbile.app.maddahyar.data.local.Lyric
import ir.vbile.app.maddahyar.data.repo.category.CategoryRepository
import ir.vbile.app.maddahyar.data.repo.lyric.LyricRepository
import ir.vbile.app.maddahyar.util.UserSharedPreferencesHelper
import kotlinx.coroutines.launch
import timber.log.Timber
import java.util.*

class MainVM @ViewModelInject constructor(
    private val lyricsRepository: LyricRepository,
    private val categoryRepository: CategoryRepository,
    private val userSP: UserSharedPreferencesHelper
) : BaseViewModel() {

    private val _lyrics: MutableLiveData<List<Lyric>> = MutableLiveData()
    val lyrics: LiveData<List<Lyric>> = _lyrics

    private val _lyricsFilteredByCategoryID: MutableLiveData<List<Lyric>> = MutableLiveData()
    val lyricsFilteredByCategoryID: LiveData<List<Lyric>> = _lyricsFilteredByCategoryID

    private val _categories: MutableLiveData<List<Category>> = MutableLiveData()
    val categories: LiveData<List<Category>> = _categories


    private val _isAuthorized: MutableLiveData<Boolean> = MutableLiveData()
    val isAuthorized: LiveData<Boolean> = _isAuthorized

    init {
        authenticateUser()
        getLyrics()
        getCategories()
    }

    private fun getLyrics() {
        progressLiveData.value = true
        viewModelScope.launch {
            lyricsRepository.getLyrics()
                .updateOnSuccess(_lyrics)
                .onSuccess {
                    progressLiveData.value = false
                }.onError {
                    Timber.e(this)
                }
        }
    }

    fun search(query: String) {
        viewModelScope.launch {
            lyricsRepository.search(query)
                .onSuccess {
                    Timber.i(this.toString())
                }.onError {
                    Timber.e(this)
                }
        }
    }

    private fun getCategories() {
        viewModelScope.launch {
            categoryRepository.getCategories()
                .updateOnSuccess(_categories)
                .onSuccess {
                    Timber.i(this.toString())
                    progressLiveData.value = false
                }.onError {
                    Timber.e(this)
                }
        }
    }

    fun setCategory(category: Category) {
        _category.postValue(category)
    }

    fun filterLyrics(categoryId: String) {
        viewModelScope.launch {
            lyricsRepository.getLyricsByCategoryId(categoryId)
                .onSuccess {
                    _lyrics.postValue(this)
                }.onError {
                    Timber.e(this)
                }.onSuccess {
                    Timber.i(this.toString())
                }
        }
    }

    private val _category: MutableLiveData<Category> = MutableLiveData()
    val category: LiveData<Category> = _category

    private fun authenticateUser() {
        if (userSP.bazaarToken.isEmpty()) {
            _isAuthorized.postValue(false)
        } else {
            getUser()
            _isAuthorized.postValue(true)
        }
    }

    fun registerUser(account: BazaarSignInAccount?) {
        val accountID = account?.accountId ?: return
        userSP.bazaarToken = accountID
        authenticateUser()
    }

    private val _account: MutableLiveData<String> = MutableLiveData()
    val account: LiveData<String> = _account

    private fun getUser() {
//        _account.postValue(userSP.bazaarToken)
    }

    fun loginUser(account: BazaarSignInAccount?) {
        val accountID = account?.accountId ?: return
        userSP.bazaarToken = accountID
        authenticateUser()
    }


    private val _isBazaarInstalled: MutableLiveData<Boolean> = MutableLiveData()
    val isBazaarInstalled: LiveData<Boolean> = _isBazaarInstalled
    fun isBazaarInstalled(context: Context) {
        val result = BazaarClientProxy.isBazaarInstalledOnDevice(context)
        if (result) {
            _isBazaarInstalled.postValue(result)
        } else
            _isBazaarInstalled.postValue(false)
    }


    private val _isNeededToUpdateBazaar: MutableLiveData<Boolean> = MutableLiveData()
    val isNeededToUpdateBazaar: LiveData<Boolean> = _isNeededToUpdateBazaar
    fun isNeededToUpdateBazaar(context: Context) {
        val isNeededToUpdateBazaar = BazaarClientProxy.isNeededToUpdateBazaar(context)
        if (isNeededToUpdateBazaar.needToUpdateForAuth) {
            _isNeededToUpdateBazaar.postValue(isNeededToUpdateBazaar.needToUpdateForAuth)
        } else {
            _isNeededToUpdateBazaar.postValue(false)
        }
    }


    fun getLyricsByCategoryId(categoryId: String) {
        viewModelScope.launch {
            lyricsRepository.getLyricsByCategoryId(categoryId)
                .onSuccess {
                    _lyricsFilteredByCategoryID.postValue(this)
                }.onError {
                    Timber.e(this)
                }.onSuccess {
                    Timber.i(this.toString())
                }
        }
    }

    private val _title: MutableLiveData<String> = MutableLiveData()
    private val title: LiveData<String> = _title
    fun setTitle(title: String?) {
        this._title.value = title
    }

    private val _content: MutableLiveData<String> = MutableLiveData()
    private val content: LiveData<String> = _content

    private val _isLyricCreatedSuccessfully: MutableLiveData<Boolean> = MutableLiveData()
    val isLyricCreatedSuccessfully: LiveData<Boolean> = _isLyricCreatedSuccessfully
    fun setContent(content: String?) {
        this._title.value = content
    }

    fun createLyric(title: String, content: String) {
        viewModelScope.launch {
            lyricsRepository.createLyric(
                title,
                content,
                category.value?.id.toString()
            ).onSuccess {
                _isLyricCreatedSuccessfully.postValue(true)
            }.onError {
                _isLyricCreatedSuccessfully.postValue(false)
                Timber.e(this)
            }
        }
    }

   /* private val lyricDataSource = object : DataSource.Factory<Int, Lyric>() {
        var source = MutableLiveData<PageKeyedDataSource<Int, Lyric>>()
        override fun create(): DataSource<Int, Lyric> = object : PageKeyedDataSource<Int, Lyric>() {
            override fun loadInitial(
                params: LoadInitialParams<Int>,
                callback: LoadInitialCallback<Int, Lyric>
            ) {
                viewModelScope.launch {
                    lyricsRepository.getLyrics(
                        pageLimit = params.requestedLoadSize
                    ).onSuccess {
                        callback.onResult(this, 0, 0)
                    }
                }
            }

            override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Lyric>) =
                Unit

            override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Lyric>) {
                viewModelScope.launch {
                    lyricsRepository.getLyrics(
                        pageLimit = params.requestedLoadSize,
                        page = params.key
                    ).onSuccess {
                        callback.onResult(this, params.key + 1)
                    }
                }
            }
        }
    }*/
//    val lyrics = lyricDataSource.toLiveData(Config(10, 10, false, 10))
//
//    fun refresh() = lyricDataSource.source.value?.invalidate() ?: Unit
//
//    fun retry() = lyricDataSource.source.value?.invalidate() ?: Unit
}