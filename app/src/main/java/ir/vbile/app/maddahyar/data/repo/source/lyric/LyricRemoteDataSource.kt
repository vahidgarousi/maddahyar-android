package ir.vbile.app.maddahyar.data.repo.source.lyric

import ir.vbile.app.maddahyar.common.Resource
import ir.vbile.app.maddahyar.data.local.Lyric
import ir.vbile.app.maddahyar.services.http.LyricsApi
import javax.inject.Inject

class LyricRemoteDataSource @Inject constructor(
    private val lyricsApi: LyricsApi
) : LyricDataSource {
    override suspend fun getLyrics(): List<Lyric> =
        lyricsApi.getLyrics().data.toLyrics()

    override suspend fun search(keyword: String): List<Lyric> =
        lyricsApi.search(keyword).data.toLyrics()

    override suspend fun getLyricsByCategoryId(categoryId: String): List<Lyric> =
        lyricsApi.getLyricsByCategoryId(categoryId).data.toLyrics()

    override suspend fun createLyric(
        title: String,
        content: String,
        categoryId: String
    ): Lyric =
        lyricsApi.createLyric(
            title,
            content,
            categoryId
        ).data.toLyric()
}