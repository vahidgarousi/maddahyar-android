package ir.vbile.app.maddahyar.feature.main

import android.content.Intent
import android.view.View
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.farsitel.bazaar.BazaarClientProxy
import com.farsitel.bazaar.auth.BazaarSignIn
import com.farsitel.bazaar.auth.BazaarSignInClient
import com.farsitel.bazaar.auth.model.BazaarSignInOptions
import com.google.android.material.tabs.TabLayout
import dagger.hilt.android.AndroidEntryPoint
import ir.vbile.app.maddahyar.R
import ir.vbile.app.maddahyar.common.BaseFragment
import ir.vbile.app.maddahyar.others.Constants.BAZAAR_AUTH_REQ_CODE
import ir.vbile.app.maddahyar.util.extentions.afterTextChanged
import kotlinx.android.synthetic.main.fragment_main.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

@AndroidEntryPoint
class MainFragment : BaseFragment<MainVM>(
    R.layout.fragment_main,
    MainVM::class
) {
    @Inject
    lateinit var lyricAdapter: LyricAdapter

    @Inject
    lateinit var spacesItemDecoration: SpacesItemDecoration

    @Inject
    lateinit var bazaarSignInOptions: BazaarSignInOptions

    @Inject
    lateinit var bazaarSignInClient: BazaarSignInClient

    override fun setupViews(view: View): View = view.apply {
        setupLyricAdapter()
        btnCreateLyric.setOnClickListener {
            findNavController().navigate(R.id.action_mainFragment_to_createNewLyricFragment)
        }
        etKeyword.afterTextChanged {
            it?.let { it1 -> vm.search(it1) }
        }
        btnAllCategories.setOnClickListener {
            findNavController().navigate(R.id.action_mainFragment_to_categoriesFragment)
        }
    }

    private fun setupCategoryTabLayoutAndAdapter() {
        vm.categories.value?.take(6)?.forEach {
            val tab = tlCategories.newTab()
                .setText(it.title)
                .setTag(it.id.toString())
            tlCategories.addTab(tab)
        }
        CoroutineScope(Dispatchers.Main).launch {
            tlCategories.getTabAt(0)?.select()
        }
        tlCategories.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                val selectedCategoryId = tab?.tag ?: return
                vm.filterLyrics(selectedCategoryId.toString())
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {}

            override fun onTabReselected(tab: TabLayout.Tab?) {}
        })
    }

    private fun setupLyricAdapter() {
        lyricAdapter.setOnItemClickedListener {
            val action = MainFragmentDirections.actionMainFragmentToShowLyricFragment(it)
            findNavController().navigate(action)
        }
        rvLyrics.apply {
            addItemDecoration(spacesItemDecoration)
            layoutManager = GridLayoutManager(requireContext(), 2)
            adapter = lyricAdapter
        }
    }

    override fun subscribeToObservers() {
        vm.isBazaarInstalled(requireContext())
        vm.isBazaarInstalled.observe(viewLifecycleOwner) {
            if (it) {
                vm.isNeededToUpdateBazaar(requireContext())
            } else
                BazaarClientProxy.showInstallBazaarView(requireContext())
        }
        vm.isNeededToUpdateBazaar.observe(viewLifecycleOwner) {
            if (it) {
                BazaarClientProxy.showUpdateBazaarView(requireContext())
            } else {
                Timber.i("")
            }
        }

        vm.lyrics.observe(viewLifecycleOwner) {
            lyricAdapter.submitList(it)
        }
        vm.progressLiveData.observe(viewLifecycleOwner) {
            setProgressIndicator(it)
        }
        vm.categories.observe(viewLifecycleOwner) {
            setupCategoryTabLayoutAndAdapter()
        }
        vm.isAuthorized.observe(viewLifecycleOwner) {
            if (!it) {
                showLoginWithBazaar()
            } else
                Timber.i("")
        }
        vm.account.observe(viewLifecycleOwner) {
            // FIXME: 12/23/2020 Send accountId to server
        }
    }

    private fun showLoginWithBazaar() {
        val intent = bazaarSignInClient.getSignInIntent()
        startActivityForResult(intent, BAZAAR_AUTH_REQ_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == BAZAAR_AUTH_REQ_CODE) {
            try {
                var account = BazaarSignIn.getSignedInAccountFromIntent(data)
                if (account == null) {
                    BazaarSignIn.getLastSignedInAccount(requireContext(), this) { response ->
                        account = response?.data
                        vm.registerUser(account)
                    }
                    showLoginWithBazaar()
                } else {
                    vm.loginUser(account)
                }
            } catch (exp: Exception) {
                Timber.e(exp)
            }
        }

    }
}