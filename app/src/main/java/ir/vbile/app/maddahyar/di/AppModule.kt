package ir.vbile.app.maddahyar.di

import android.app.Activity
import android.content.Context
import com.farsitel.bazaar.auth.BazaarSignIn
import com.farsitel.bazaar.auth.BazaarSignInClient
import com.farsitel.bazaar.auth.model.BazaarSignInOptions
import com.farsitel.bazaar.auth.model.SignInOption
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.android.qualifiers.ActivityContext
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.android.scopes.ActivityScoped
import ir.vbile.app.maddahyar.R
import ir.vbile.app.maddahyar.feature.category.CategoriesAdapter
import ir.vbile.app.maddahyar.feature.lyric.create.CategoryAdapter
import ir.vbile.app.maddahyar.feature.main.LyricAdapter
import ir.vbile.app.maddahyar.feature.main.SpacesItemDecoration

@Module
@InstallIn(ActivityComponent::class)
object AppModule {
    @Provides
    fun provideLyricAdapter() = LyricAdapter()

    @Provides
    @ActivityScoped
    fun provideSpacesItemDecoration(
        @ApplicationContext context: Context
    ) = SpacesItemDecoration(
        context.resources.getDimensionPixelSize(R.dimen.spacing)
    )

    @Provides
    @ActivityScoped
    fun provideCategoryCreateLyricAdapter() = CategoryAdapter()


    @Provides
    @ActivityScoped
    fun provideCategoriesAdapter() = CategoriesAdapter()


    @Provides
    @ActivityScoped
    fun provideBazaarSignInOptions(): BazaarSignInOptions =
        BazaarSignInOptions.Builder(SignInOption.DEFAULT_SIGN_IN).build()

    @Provides
    @ActivityScoped
    fun provideBazaarSignInClient(
        @ActivityContext context: Context,
        bazaarSignInOptions: BazaarSignInOptions
    ) : BazaarSignInClient = BazaarSignIn.getClient(context as Activity, bazaarSignInOptions)
}