package ir.vbile.app.maddahyar.data.repo.lyric

import ir.vbile.app.maddahyar.common.BaseRepository
import ir.vbile.app.maddahyar.common.Resource
import ir.vbile.app.maddahyar.data.local.Lyric

interface LyricRepository : BaseRepository {
    suspend fun getLyrics(): Resource<List<Lyric>>
    suspend fun search(keyword: String): Resource<List<Lyric>>
    suspend fun getLyricsByCategoryId(categoryId: String): Resource<List<Lyric>>
    suspend fun createLyric(title: String, content: String, categoryId: String): Resource<Lyric>
}