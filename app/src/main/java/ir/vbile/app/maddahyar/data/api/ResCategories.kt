package ir.vbile.app.maddahyar.data.api


import kotlinx.android.parcel.Parcelize
import android.os.Parcelable
import com.squareup.moshi.JsonClass
import ir.vbile.app.maddahyar.data.local.Category

@Parcelize
@JsonClass(generateAdapter = true)
data class ResCategories(
    val categories: List<ApiCategory?>?
) : Parcelable {
    fun toCategories(): List<Category> = (categories ?: listOf()).map {
        it?.toCategory() ?: Category(
            -1,
            "",
            ""
        )
    }

    @Parcelize
    @JsonClass(generateAdapter = true)
    data class ApiCategory(
        val id: Int?,
        val title: String?,
        val image: String?
    ) : Parcelable {
        fun toCategory() = Category(
            id ?: -1,
            title ?: "",
            image ?: ""
        )
    }
}