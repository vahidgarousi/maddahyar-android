package ir.vbile.app.maddahyar.data.local

import android.os.Parcelable
import ir.vbile.app.maddahyar.data.api.ResLyrics
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Lyric(
    val id: Int,
    val category: Category,
    val content: String,
    val title: String
) : Parcelable {
    @Parcelize
    data class Category(
        val id: String,
        val image: String,
        val title: String
    ) : Parcelable
}