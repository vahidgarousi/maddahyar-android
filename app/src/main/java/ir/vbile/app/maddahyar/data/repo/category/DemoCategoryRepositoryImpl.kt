package ir.vbile.app.maddahyar.data.repo.category

import ir.vbile.app.maddahyar.common.Resource
import ir.vbile.app.maddahyar.data.local.Category
import ir.vbile.app.maddahyar.data.repo.source.category.CategoryDataSource
import javax.inject.Inject

class DemoCategoryRepositoryImpl : CategoryRepository {
    override suspend fun getCategories(): Resource<List<Category>> = safeApiCall {
        TODO("Not yet implemented")
    }
}