package ir.vbile.app.maddahyar.feature.category

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import ir.vbile.app.maddahyar.R
import ir.vbile.app.maddahyar.common.BaseViewHolder
import ir.vbile.app.maddahyar.data.local.Category
import kotlinx.android.synthetic.main.item_category_categories.view.*
import javax.inject.Inject

class CategoriesAdapter @Inject constructor() : ListAdapter<Category, BaseViewHolder>(diffUtil) {
    private var listener: (Category) -> Unit = {}

    fun setOnItemClickedListener(listener: (Category) -> Unit) {
        this.listener = listener
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return CategoryViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_category_categories, parent, false)
        )
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) = holder.bind()

    inner class CategoryViewHolder(itemView: View) : BaseViewHolder(itemView) {
        override fun bind(position: Int) {
            val category = getItem(position) ?: return
            itemView.apply {
                tvTitle.text = category.title
                setOnClickListener {
                    listener.invoke(category)
                }
            }
        }
    }

    companion object {
        private val diffUtil = object : DiffUtil.ItemCallback<Category>() {
            override fun areItemsTheSame(oldItem: Category, newItem: Category): Boolean =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: Category, newItem: Category): Boolean =
                oldItem.hashCode() == newItem.hashCode()
        }
    }
}