package ir.vbile.app.maddahyar.data.repo.category

import ir.vbile.app.maddahyar.common.Resource
import ir.vbile.app.maddahyar.data.local.Category
import ir.vbile.app.maddahyar.data.repo.source.category.CategoryDataSource
import javax.inject.Inject

class CategoryRepositoryImpl @Inject constructor(
    private val remoteDataSource: CategoryDataSource,
    private val localDataSource: CategoryDataSource
) : CategoryRepository {
    override suspend fun getCategories(): Resource<List<Category>> = safeApiCall {
        remoteDataSource.getCategories()
    }
}